import {GET_ARTICLES} from "../actionTypes";

const initialState = {
  articles: []
};

const articles = (state = initialState, action) => {
  switch (action.type) {
    case GET_ARTICLES: {
      return action.articles;
    }
    default: {
      return state;
    }
  }
};

export default articles;
