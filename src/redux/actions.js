import axios from 'axios';
import { GET_ARTICLES } from "./actionTypes";
import apiURL from '../../src/config/APIUrls';

export const fetchArticles = (articles) => {
  return {
    type: GET_ARTICLES,
    articles
  }
};

export const fetchAllArticles = () => {
  return (dispatch) => {
    fetchAllArticlesAsync()
      .then((data: any) => {
        return dispatch(fetchArticles(data.data.items));
      })
      .catch((error: any) => {
        console.error(error);
      });
  };
};

async function fetchAllArticlesAsync() {
  try {
    const response = await axios.get(apiURL.rss_abc_news_feed);
    const body = await response;
    return body;
  } catch (error) {
    return error;
  }
}
