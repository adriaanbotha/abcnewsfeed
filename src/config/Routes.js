// @flow
import ArticlesList from "../containers/ArticlesList";
import Article from "../containers/Article";

const Routes = {
  Articles: {
    screen: ArticlesList,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  Article: {
    screen: Article,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
};

export default Routes;
