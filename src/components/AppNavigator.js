import { createStackNavigator, createAppContainer } from 'react-navigation'
import Articles from '../containers/ArticlesList'
import Article from '../containers/Article'

const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: Articles,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    Article: {
      screen: Article,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    }
  }
);


export default createAppContainer(AppNavigator);
