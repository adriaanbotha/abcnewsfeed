import React from 'react'
import {View, StyleSheet, Text} from 'react-native'
import PropTypes from 'prop-types'

const Toolbar = ({
   title
  }) => {
  return (
    <View style={styles.content}>
      {/*<Text style={styles.leftMenu}>leftMenu</Text>*/}
      <Text style={styles.title}>{title}</Text>
    </View>
  )
}

Toolbar.propTypes = {
  title: PropTypes.String
}

const styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    padding: 20,
    backgroundColor: '#FFF',
    width: '100%',
    shadowColor: '#000',
  },
  title: {
    padding: 5,
    fontFamily: 'Helvetica Neue',
    fontWeight: '800',
    fontSize: 25
  },
  leftMenu: {}
})
export default Toolbar
