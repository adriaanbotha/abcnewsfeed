import React from 'react'
import {View, StyleSheet, Text, Image, Dimensions} from 'react-native'

const w = Dimensions.get('window');
const noLines = 3;  //More or less 3 lines for title to be displayed

const Card = ({
                image,
                title,
                pubDate,
                primary
              }) => {
  console.log('image url ->', image);

  return (
    <View style={primary === true ? styles.contentPrimary: styles.contentNormal}>
      <Image style={primary === true ? styles.imagePrimary: styles.imageNormal} source={{ uri: `${image}` }}/>
      <View style={styles.textBox}>
        <Text style={styles.headline} numberOfLines={noLines}>{title}</Text>
        <Text style={styles.datetime}>{pubDate}</Text>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  contentPrimary: {
    marginTop: 10,
    backgroundColor: '#FFF',
    width: '100%',
    shadowColor: '#000',
    borderRadius: 5,
  },
  contentNormal: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    marginTop: 10,
    backgroundColor: '#FFF',
    width: '100%',
    shadowColor: '#000',
    borderRadius: 5,
  },
  textBox: {
    width: '65%',

  },
  datetime: {
    color: '#838383',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 15,
    paddingBottom: 10,
    fontFamily: 'Helvetica Neue',
    fontWeight: '400',
    fontSize: 12,
  },
  headline: {
    fontFamily: 'Helvetica Neue',
    fontWeight: '600',
    fontSize: 20,
    color: '#3d7ea7',
    paddingLeft: 10,
    paddingRight: 15,
    paddingTop: 15,
    paddingBottom: 10,
  },
  imagePrimary: {
    borderRadius: 5,
    width: '100%',
    height: w.height/4,
    backgroundColor: '#ff8616',
  },
  imageNormal: {
    borderRadius: 5,
    width: '33%',
    backgroundColor: '#ff8616',
  }
});
export default Card
