import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet, Text, View, WebView} from "react-native";

function Article({ownProps}) {

  console.log("article ->", ownProps.navigation.state.params.article);


  if (!(ownProps.navigation)) {
    return (
      <View style={styles.noArticles}>
        <Text>Article details not Found</Text>
      </View>
    )
  }
  const articleUrl = ownProps.navigation.state.params.article.link;
  return (
    <WebView
      source={{uri: articleUrl}}
      style={{marginTop: 20}}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    padding: 10,

  },
  noArticles: {
    backgroundColor: '#d3bad8',
    height: '10%',
    padding: 20,
  }
});

const mapStateToProps = (state, ownProps) => {
  return {
    ownProps: ownProps
  };
};

export default connect(
  mapStateToProps
)(Article);
