import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet, Text, View, ScrollView, TouchableOpacity} from "react-native";
import Card from "../components/Card";

function ArticlesList({articles, ownProps}) {

  if (!articles.length) {
    return (
      <View style={styles.noArticles}>
        <Text>No Articles Found - perhaps try again later.</Text>
      </View>
    )
  }
  const _handleArticlePress = (article) => {
    ownProps.navigation.navigate('Article', { article })
  }

  return (
    <ScrollView style={styles.container} directionalLockEnabled={true}>
      {articles.map((article, index) => {
        //For large top article, show enclosure link and different image size
        if (index === 0) {
          return (
            <TouchableOpacity
              style={styles.button}
              onPress={() => _handleArticlePress (article)}
            >
              <Card
                key = {index}
                image={article.enclosure.link}
                title={article.title}
                pubDate={article.pubDate}
                primary
              />
            </TouchableOpacity>)
        }
        return (
          <TouchableOpacity
            style={styles.button}
            onPress={() => _handleArticlePress (article)}
          >
            <Card
              key = {index}
              image={article.thumbnail}
              title={article.title}
              pubDate={article.pubDate}
            />
          </TouchableOpacity>)
      })}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    padding: 10,

  },
  noArticles: {
    backgroundColor: '#d3bad8',
    height: '10%',
    padding: 20,
  }
});

const mapStateToProps = (state, ownProps) => {
  return {
    articles: state.articles,
    ownProps: ownProps
  };
};

export default connect(
  mapStateToProps
)(ArticlesList);
