/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {Provider} from "react-redux";
import store from "../redux/store";
import {fetchAllArticles} from '../redux/actions';
import ArticlesList from './ArticlesList';
import Toolbar from "../components/Toolbar";
import AppNavigator from "../components/AppNavigator";


type Props = {};
export default class App extends Component<Props> {
  componentWillMount(): void {
    store.dispatch(fetchAllArticles());
  }

  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <Toolbar title={"News Feed"}/>
          <AppNavigator />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#d8d8d8',
    height: '100%'
  }
});
